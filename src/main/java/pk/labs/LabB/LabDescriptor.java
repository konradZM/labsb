package pk.labs.LabB;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabB.Implementation.display";
    public static String controlPanelImplClassName = "pk.labs.LabB.Implementation.controlPanel";

    public static String mainComponentSpecClassName = "pk.labs.LabB.Contracts.IMyjnia";
    public static String mainComponentImplClassName = "pk.labs.LabB.Implementation.Myjnia";
    public static String mainComponentBeanName = "mojamyjnia";
    // endregion

    // region P2
    public static String mainComponentMethodName = "...";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "...";
    // endregion
}
